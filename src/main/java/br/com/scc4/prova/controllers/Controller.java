package br.com.scc4.prova.controllers;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

	@GetMapping(path = "/listaReversa")
	private String listaReversa(@RequestParam(name = "lista") int[] list) {
		System.out.println(list.toString());
		int size = list.length;
		int[] newList = new int[list.length];

		for (int i = 0; i < newList.length; i++) {
			size--;
			newList[i] = list[size];
		}

		return Arrays.toString(newList);
	}

	@GetMapping(path = "/imprimirImpares")
	private String imprimirImpares(@RequestParam(name = "lista") int[] list) {

		ArrayList<Integer> newList = new ArrayList<Integer>();
		for (int i = 0; i < list.length; i++) {
			if (list[i] % 2 == 1) {
				newList.add(list[i]);
			}
		}

		return newList.toString();
	}

	@GetMapping(path = "/imprimirPares")
	private String imprimirPares(@RequestParam(name = "lista") int[] list) {

		ArrayList<Integer> newList = new ArrayList<Integer>();
		for (int i = 0; i < list.length; i++) {
			if (list[i] % 2 == 0) {
				newList.add(list[i]);
			}
		}

		return newList.toString();
	}

	@GetMapping(path = "/tamanho")
	private String imprimirTamanho(@RequestParam(name = "palavra") String word) {
		return "Tamanho=" + String.valueOf(word.length());
	}

	@GetMapping(path = "/maiusculas")
	private String toUpperCase(@RequestParam(name = "palavra") String word) {
		return word.toUpperCase();
	}

	@GetMapping(path = "/vogais")
	private String vogals(@RequestParam(name = "palavra") String word) {
		return word.replaceAll("[^a|e|i|o|u|A|E|I|O|U]", "");
	}

	@GetMapping(path = "/consoantes")
	private String consoantes(@RequestParam(name = "palavra") String word) {
		return word.replaceAll("a|e|i|o|u|A|E|I|O|U", "");
	}

	@GetMapping(path = "/nomeBibliografico")
	private String nomeBibliografico(@RequestParam(name = "nome") String word) {
		String[] words = word.split("%");
		StringBuilder stringFormatada = new StringBuilder("");

		for (int i = 0; i < words.length; i++) {
			words[i] = words[i].substring(0, 1).toUpperCase() + words[i].substring(1);

			if (words.length - 1 == i) {
				stringFormatada.insert(0, words[i].toUpperCase().toUpperCase() + ", ");
			} else {
				stringFormatada.append(words[i] + " ");
			}
		}
		return stringFormatada.toString();
	}

	@GetMapping(path = "/saque")
	private String saque(@RequestParam(name = "valor") int valor) {

		int saque = valor;

		int notasDeCinco = saque / 5;
		int notasDeTres = 0;

		int resto = saque - (notasDeCinco * 5);

		while (resto != 0) {
			if (resto % 3 == 0) {
				notasDeTres = resto / 3;
				resto = 0;
			}

			if (resto == 0) {
				break;
			}

			notasDeCinco -= 1;
			resto += 5;
		}

		return "Notas de R$5 = " + notasDeCinco + "\n" + "Notas de R$3 = " + notasDeTres;

	}

}
